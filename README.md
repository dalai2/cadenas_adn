# Repositorio de la prueba técnica de verificación de cadenas
1.-Para correr este repositorio clonalo en el directorio deseado
2.-Crea un ambiente virtual y activalo:
    *WIndows* `python -m venv venv`
    `venv/Scripts/activate`
    *Linux/mac* `python3 -m venv venv`
    `source venv/bin/activate`
3.- Una vez activado el ambiente virtual instala los paquetes necesarios con `pip install requirements.txt`
4.- Cambia el nombre de .env-ejemplo por .env para establecer la llave secreta de prueba en el proyecto
5.- Corre las migraciones con `python3 manage.py migrate --run-syncdb`
6.- Activa el servidor con `python3 manage.py runserver`
7.- Puedes visitar /POST para hacer una petición

## El archivo cadenas.py dentro de adn_verifier
    Dentro de este archivo viene la solución al reto para encontrar cadenas repetidas de ADN, vertical u horizontalmente. El script llevará la cuenta de los errores géneticos y devolverá verdadero en caso de evaluar 2 o mas apariciones de 4 letras seguidas ocupando expresiones regulares.

### Url de la app
La aplicación fue puesta en producción en: "http://django-env.eba-vtj2aac4.us-west-2.elasticbeanstalk.com/"
lamentablemente no funciona, pues devuelve un error 502 
