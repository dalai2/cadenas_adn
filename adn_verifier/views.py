from django.http import HttpResponse, QueryDict
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from rest_framework import status
from .models import ADN_Model
from .serializers import Adn_serializer
import re
import ast
import pandas as pd
import io
class ADN_View(ListCreateAPIView):
    queryset = ADN_Model.objects.all()
    serializer_class = Adn_serializer


    def post(self,request):
        serializer = Adn_serializer(data=request.data)
        if serializer.is_valid():

            adn = re.search("\[.*\]",serializer['cadenas'])
            adn = ast.literal_eval(adn)

            contador_mutacion = 0 # llevar la cuenta de las mutaciones
            for i in adn: 
                    #Busca cualquier aparicion consecutiva de las 4 letras posibles 
                evaluar_cadena = re.findall("[A]{4}|[T]{4}|[C]{4}|[G]{4}|",i)
                if len(evaluar_cadena[0]): # Por cada aparicion incrementa el contador en 1
        
                    contador_mutacion +=1
                    
            """ Se crea una matriz de los caracteres para reacomodarlos en una matriz transpuesta para evaluar las apariciones  """
            data = []
            df = adn
            for i in df: #se crea matrix inicial
                data.append(list(i))
            data = pd.DataFrame(data)
            df_transpuesto = data.transpose() # Se transpone la matriz
            cadena_transpuesta = [''.join(val) for val in df_transpuesto.astype(str).values.tolist()] #Se aplana para ser evaluada 
            for i in cadena_transpuesta: #Busca cualquier aparicion consecutiva de las 4 letras posibles 
                evaluar_cadena = re.findall("[A]{4}|[T]{4}|[C]{4}|[G]{4}|",i)
                
                if len(evaluar_cadena[0]): # Por cada aparicion incrementa el contador en 1
                    contador_mutacion +=1
            if contador_mutacion > 2:
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=403)
