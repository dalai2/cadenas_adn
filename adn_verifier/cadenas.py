import re
import pandas as pd
adn_novalido = ["ATGCGghjhjA", "CAGTGC", "TTATbT", "AfgfdTAAGG", "CCCCTA",
       "TCACTG"]
adn_valido = ["ATGCGA", "CAGTGC", "TATTGT", "AAAAGG", "CCCCTA", "TCACTG"]

def crear_matriz(adn):
    data = []
    df = adn
    for i in df:
        
        data.append(list(i))

    data = pd.DataFrame(data)
    return data
def cadena_valida(adn):

    # valida si la cadena de adn
    # contiene unicamente los valores permitidos.
    
    cadena = (''.join(adn_valido))
    
    evaluar_cadena = re.findall("[^ATCG]", cadena)
    if evaluar_cadena :
        return cadena
    else :
        return cadena


"""Funcion para evaluar mutaciones en cadenas de ADN"""
def has_mutation(adn):
    contador_mutacion = 0 # llevar la cuenta de las mutaciones
    for i in adn: #Busca cualquier aparicion consecutiva de las 4 letras posibles 
        print(i)
        evaluar_cadena = re.findall("[A]{4}|[T]{4}|[C]{4}|[G]{4}|",i)
        
        if len(evaluar_cadena[0]): # Por cada aparicion incrementa el contador en 1
            contador_mutacion +=1
            print(contador_mutacion)
    """ Se crea una matriz de los caracteres para reacomodarlos en una matriz transpuesta para evaluar las apariciones  """
    data = []
    df = adn
    for i in df: #se crea matrix inicial
        
        data.append(list(i))

    data = pd.DataFrame(data)
    df_transpuesto = data.transpose() # Se transpone la matriz
    print(df_transpuesto)
    cadena_transpuesta = [''.join(val) for val in df_transpuesto.astype(str).values.tolist()] #Se aplana para ser evaluada 
    for i in cadena_transpuesta: #Busca cualquier aparicion consecutiva de las 4 letras posibles 
        print(i)
        evaluar_cadena = re.findall("[A]{4}|[T]{4}|[C]{4}|[G]{4}|",i)
        
        if len(evaluar_cadena[0]): # Por cada aparicion incrementa el contador en 1
            contador_mutacion +=1
            print(contador_mutacion)
    if contador_mutacion > 2:
        return True:
    else:
        return False

has_mutation(adn_valido)
#crear_matriz(adn_valido)