from rest_framework import serializers
from .models import ADN_Model

class Adn_serializer(serializers.ModelSerializer):
    class Meta:
        model=ADN_Model
        fields= ['cadenas']

