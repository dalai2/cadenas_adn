from django.urls import path

from .views import ADN_View


app_name = "adns"

urlpatterns = [
    path('', ADN_View.as_view()),
]